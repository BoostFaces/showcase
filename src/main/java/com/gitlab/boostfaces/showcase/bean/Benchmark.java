/*
 * Copyright 2019 BoostFaces
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.gitlab.boostfaces.showcase.bean;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class Benchmark implements Serializable {

	private static final long serialVersionUID = -7416750215595497498L;

	/**
	 * Number of increments.
	 */
	private int n = 0;

	private Instant measurementStartTime = Instant.now();

	private int measurementStartValue = 0;

	private double measurementResult = 0.0;

	private final int MEASUREMENT_TIME_SLICE_SECONDS = 5;

	/**
	 * Indicates whether n should be incremented in a loop.
	 */
	private boolean loop = false;

	/**
	 * Gets the Number of increments.
	 * 
	 * @return the Number of increments.
	 */
	public int getN() {
		return n;
	}

	/**
	 * Increments.
	 */
	public void increment() {
		n++;
		measure();
	}

	public boolean getLoop() {
		return loop;
	}

	public void toggleLoop() {
		loop = !loop;
	}

	public double getMeasurementResult() {
		return measurementResult;
	}

	private void measure() {
		final Instant now = Instant.now();
		if (now.isAfter(measurementStartTime.plus(Duration.ofSeconds(MEASUREMENT_TIME_SLICE_SECONDS)))) {
			final double elapsedSeconds = Duration.between(measurementStartTime, now).toMillis() / 1000.0;
			measurementResult = ((double) n - measurementStartValue) / elapsedSeconds;
			measurementStartTime = now;
			measurementStartValue = n;
		}
	}
}
