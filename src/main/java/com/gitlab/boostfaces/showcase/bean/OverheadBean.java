/*
 * Copyright 2019 BoostFaces
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.gitlab.boostfaces.showcase.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.sun.faces.component.visit.FullVisitContext;

@Named
@ViewScoped
public class OverheadBean implements Serializable {

	private static final long serialVersionUID = 6440477108395532718L;

	private int overheadComponentCount = 500;

	@PostConstruct
	public void init() {
		applyCompCount();
	}

	/**
	 * Apply overheadComponentCount, {@link #overheadComponentCount} number of
	 * component and adding them to the current view.
	 */
	public void applyCompCount() {
		final UIComponent overheadComponent = getOverheadContainer();
		overheadComponent.getChildren().clear();
		for (int i = 0; i < overheadComponentCount; i++) {
			final UIComponent newChild;
			if (i % 3 == 0) {
				newChild = new HtmlCommandButton();
			} else if (i % 5 == 0) {
				newChild = new HtmlInputText();
			} else if (i % 2 == 0) {
				newChild = new HtmlSelectOneMenu();
			} else {
				newChild = new HtmlOutputText();
			}
			overheadComponent.getChildren().add(newChild);
		}
	}

	private UIComponent getOverheadContainer() {
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		final VisitContext visistContext = new FullVisitContext(facesContext);
		final List<UIComponent> matches = new ArrayList<UIComponent>(1);
		final VisitCallback callback = (ctx, component) -> {
			if ("pnlOverhead".equals(component.getId())) {
				matches.add(component);
				return VisitResult.COMPLETE;
			}
			return VisitResult.ACCEPT;
		};
		facesContext.getViewRoot().visitTree(visistContext, callback);
		assert matches.size() == 1;

		return matches.get(0);
	}

	public int getOverheadComponentCount() {
		return overheadComponentCount;
	}

	public void setOverheadComponentCount(final int overheadComponentCount) {
		this.overheadComponentCount = overheadComponentCount;
	}

}
